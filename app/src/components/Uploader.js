import React, { Component } from 'react';
import {ipfs} from '../ipfs.js'
const Room = require('ipfs-pubsub-room')
class Uploader extends Component {

  constructor (props: any) {
    super(props)
    this.state = {
      buffer: '',
      status:'start'
    }
    this.room={}
  }

  componentDidMount(){
    //
  }


  captureFile =(event) => {
        const file = event.target.files[0]
        let reader = new window.FileReader()
        reader.readAsArrayBuffer(file)
        reader.onloadend = () => this.convertToBuffer(reader)    
  }

  convertToBuffer = async(reader) => {
      //file is converted to a buffer to prepare for uploading to IPFS
        const buffer = await Buffer.from(reader.result);
      //set this buffer -using es6 syntax
        this.setState({buffer,status:'file loaded'});
  }

  async uploadToIPFS (buffer) {
    console.log('\n-------------- Storing string on IPFS : --------------')
    try {
      // use ipfs-api to add file
      let response = await ipfs.add([buffer], {pin: true})

      // log path of stored file
      let path = response[0].path
      console.log('file path : ', path)

      // verify that description is correctly stord and log it
      let verif = await ipfs.get(path)
      //console.log(verif[0].content)

      this.setState({buffer,status:'file uploaded to ipfs :'+path});
      return path
    } catch (e) {
      console.log('Error while uploading to ipfs:', e)
      this.setState({buffer,status:'error during upload to ipfs'});
    }
  }

  async upload(e){
    let hash=await this.uploadToIPFS(this.state.buffer)
    await this.publish(hash)
  }

  async publish(hash){
    // let topic='music sample'
    // ipfs.pubsub.publish(topic, hash, (err) => {
    //   if (err) {
    //     return console.error(`failed to publish to ${topic}`, err)
    //   }
    //   // msg was broadcasted
    //   console.log(`published to ${topic}`)
    // })
    // let pubsubHash=await ipfs.pubsub.publish('music sample', hash)
    // console.log('pubsubHash',pubsubHash)
  }

  render() {
    return (
      <div className="Uploader">
        <input 
          type = "file"
          onChange = {this.captureFile}/>
        <div onClick={this.upload.bind(this)} > 
               Send it 
        </div>
        <div>{'status : '+this.state.status}</div>
      </div>
    );
  }
}

export default Uploader;
