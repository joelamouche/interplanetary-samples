const IPFS = require('ipfs-http-client')

export const ipfs = new IPFS({
  //repo:repo(),
  host: 'ipfs.infura.io', port: 5001, protocol: 'https',
  EXPERIMENTAL: {
    pubsub: true
  },
  // config: {
  //   Addresses: {
  //     Swarm: [
  //        '/ip4/127.0.0.1/tcp/'+env
  //      ]
  //   }
  // }
})