const assert = require('assert');
const relayerServer= require('../relayerServer.js')
const request = require('request');


describe('Relayer Server', function() {
  describe('instantiate server on 3000', function() {
    it('should instantiate and work on port 3000', function() {
	  let server=new relayerServer(3000,()=>{
	  	let port=server.getPort()
      	assert.equal(port, 3000);
	  })
    });
	it('listen to /newSample requests and receive test string', function() {
		setTimeout(()=>{
			var postData='test string 101'
	    	request({
	    		url:'http://localhost:3000/newSample',
	    		method:'post',
	    	}, function (error, response, body) {
			  console.log('client error:', error); // Print the error if one occurred
			  console.log('client statusCode:', response && response.statusCode); // Print the response status code if a response was received
			  console.log('client body:', body); // Print the HTML for the Google homepage.
			  assert.equal(postData, body);
			}).write(postData);
		},4000)
	})
	it('broadcast test string to pubsub room and save it when received', function() {
		setTimeout(()=>{
			var postData='test string 101'
			let savedList=server.getHashList()
			console.log("savedList",savedList)
			assert.equal(savedList[savedList.length-1], postData);
			done();
		},8000)
	})
  });
});
