const Room = require('ipfs-pubsub-room')
const IPFS = require('ipfs')

class pubsubNode {
  constructor(cb) {
    this.room={}
    this.hashList=[]
    this.ipfs = new IPFS({
      repo:'ipfs/musicSamples',
      EXPERIMENTAL: {
        pubsub: true
      },
      config: {
        Addresses: {
          Swarm: [
            '/dns4/ws-star.discovery.libp2p.io/tcp/443/wss/p2p-websocket-star'
          ]
        }
      }
    })

    // IPFS node is ready, so we can start using ipfs-pubsub-room
    this.ipfs.on('ready', () => {
      this.room = Room(this.ipfs, 'music-samples')

      this.room.on('peer joined', (peer) => {
        console.log('Peer joined the room', peer)
      })

      this.room.on('peer left', (peer) => {
        console.log('Peer left...', peer)
      })

      this.room.on('message', (rawMessage) => {
        console.log("raw message",rawMessage)
        let message=Buffer.from(rawMessage.data, 'hex').toString('utf8')
        console.log("message",message)
      })

      // now started to listen to room
      this.room.on('subscribed', () => {
        console.log('Now connected to music-samples room!')
        cb()
      })
    })
   }

  broadcast(json){
    console.log('broadcast')
    let messageString=JSON.stringify(json)
    return this.room.broadcast(messageString)
  }
}

module.exports=pubsubNode