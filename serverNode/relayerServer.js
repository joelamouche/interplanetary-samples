const express = require('express')
const port = 3000
const pubsubNode= require ('./ipfs-pubsub.js')

class relayerServer {
	constructor(port,cb) {
	    this.port = port
	    this.pubsubReady=false
	    this.serverReady=false
	    this.pubsub= new pubsubNode(()=>{
	    	console.log("pubsub node connected to server")
	    	this.pubsubReady=true
	    	if (this.serverReady&&this.pubsubReady){cb()}
	    })
	    this.app= express()

		this.app.get('/', (req, res) => res.send('Hello World!'))
		this.app.post('/newSample', (req, res) => {
			console.log("New Sample posted ! ")

			let body=""
		    req.on('data', chunk => {
		    	console.log("data!")
		        body += chunk.toString(); // convert Buffer to string
		    });
		    req.on('end',()=>{
		    	this.pubsub.broadcast(body)
		    	res.send(body)
		    })
		})

		this.app.listen(this.port, () => {
			console.log(`Relayer Server listening on port ${this.port}!`)
	    	this.serverReady=true
	    	if (this.serverReady&&this.pubsubReady){cb()}
		})
	 }

	getPort(){
		return this.port
	}

	getHashList(){
		return this.pubsub.getHashList()
	}
}

module.exports=relayerServer