# Interplanetary Samples

Use a decentralized database (namely IPFS) to share and classify music samples for music production.

*Please don't hesitate to open an issue and give feedback on architecture*

## Manifesto

### Why we need a decentralized database for music samples

- most samples are and should be royalty-free
- we can't depend on a centralized server for fear of losing everything
- we need a tool to organize, rank, classify samples, maybe also something like a package manager

### Similar projects

https://splice.com/
https://sampleswap.org/
https://sounds.com/
https://samples.landr.com/

## Architecture

NB: pubsub is not currently supported (or poorly supported) for browser. Therefore, upload/download is assured through the js-ipfs api, but propagation of the hashes of the music sample collection is done through nodes/server (the *relayer server*) that do support the pubsub protocol.

### A Browser Client

*in /app*

- to upload music sample
- to browse the database

**Stack**

- React webApp
- js-ipfs to upload file
- connect to relayer api through http (and/or socket and/or libp2p)

### A Relayer Server (pubsub)

*in /serverNode*

- to upload and propagate ipfs-hashes of music samples
- to listen to and save list of music samlpe hashes
- to send list to a client

**Stack**

- express
- ipfs-pubsub
- probably some kind of database

### What's next

- a cli-client to achieve more complex tasks like analyzing large datasets of samples and uploading them with the correct labels
- relayers could implement incentivization for file storage and sample reputation
